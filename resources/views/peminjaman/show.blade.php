@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-5"> 
    <h5> Anggota </h5>
    <p> {{$peminjaman->anggota_id}} </p>
    <h5> Buku </h5>
    <p> {{$peminjaman->buku_id}} </p>
    <h5> Tanggal Peminjaman </h5>
    <p> {{$peminjaman->tanggal_pinjam}} </p>
    <h5> Tanggal Pengembalian </h5>
    <p> {{$peminjaman->tanggal_pengembalian}} </p>
</div>
    
@endsection