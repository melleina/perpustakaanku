@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3 mr-3">
    <div class="card-header px-5">
        <h3 class="card-title">Buat Data Peminjaman</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/peminjaman" method="POST">
    @csrf
        <div class="card-body px-5">
            <div class="form-group">
                <label for="tanggal_pinjam">Tanggal Peminjaman</label>
                <input type="date" id="tanggal_pinjam" name="tanggal_pinjam" value="{{old('tanggal_pinjam', '')}}">
                @error('tanggal_pinjam')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tanggal_pengembalian">Tanggal Pengembalian</label>
                <input type="date" id="tanggal_pengembalian" name="tanggal_pengembalian" value="{{old('tanggal_pengembalian', '')}}">
                @error('tanggal_pengembalian')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="buku">Buku</label>
                <select name="buku_id" class="form-control">
                    <option>--Pilih--</option>
                    @foreach($buku1 as $item)
                    <option value="{{$item->id}}">{{$item->nama_buku}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="anggota">Anggota</label>
                <select name="anggota_id" class="form-control">
                    <option>--Pilih--</option>
                    @foreach($anggota1 as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
            </div>
    <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat</button>
        </div>
    </form>
</div>
@endsection