@extends('adminlte.master')

@section('content')
<div class="row ml-3 mt-3 mr-3">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-maroon">
              <div class="inner">
                <!--h3>150</h3-->
                <h3>Data</h3>
                <h4>Anggota</h4>
              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a href="{{route('anggota.index')}}" class="small-box-footer">Details <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-teal">
              <div class="inner">
                <!--h3>53<sup style="font-size: 20px">%</sup></h3-->
                <h3>Data</h3>
                <h4>Buku</h4>
              </div>
              <div class="icon">
                <i class="ion ion-document-text"></i>
              </div>
              <a href="{{route('buku.index')}}" class="small-box-footer">Details <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-purple">
              <div class="inner">
                <!--h3>44</h3-->
                <h3>Data</h3>
                <h4>Peminjaman</h4>
              </div>
              <div class="icon">
                <i class="ion ion-minus-circled"></i>
              </div>
              <a href="{{route('peminjaman.index')}}" class="small-box-footer">Details <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-gray dark">
              <div class="inner">
                <!--h3>65</h3-->
                <h3>Data</h3>
                <h4>Pengembalian</h4>
              </div>
              <div class="icon">
                <i class="ion ion-plus-circled"></i>
              </div>
              <a href="{{route('pengembalian.index')}}" class="small-box-footer">Details <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
@endsection