<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{asset('/adminlte/dist/img/perpus.png')}}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SIPUMTA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('/adminlte/dist/img/user.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"> {{Auth::user()->name}} </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">MAIN NAVIGATION</li>
          <li class="nav-item">
            <a href="/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Profil
              </p>
            </a>
          </li>
          
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-folder"></i>
              <p>
                Kelola Data
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item ml-3">
                <a href="{{route('anggota.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Anggota</p>
                </a>
              </li>
              
              <li class="nav-item has-treeview ml-3">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-book"></i>
                  <p>
                    Buku
                  <i class="right fas fa-angle-left"></i>
                  </p>
                </a>

                <ul class="nav nav-treeview">
                  <li class="nav-item ml-3">
                    <a href="{{route('buku.index')}}" class="nav-link">
                      <i class="nav-icon fas fa-list"></i>
                      <p>Daftar Buku</p>
                    </a>
                  </li>

                  <li class="nav-item ml-3">
                    <a href="{{route('kategori.index')}}" class="nav-link">
                      <i class="nav-icon fas fa-columns"></i>
                      <p>Kategori</p>
                    </a>
                  </li>

                  <li class="nav-item ml-3">
                    <a href="{{route('rak.index')}}" class="nav-link">
                      <i class="nav-icon fas fa-table"></i>
                      <p>Rak</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Transaksi
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item ml-3">
                <a href="{{route('peminjaman.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-minus"></i>
                  <p>Peminjaman</p>
                </a>
              </li>

              <li class="nav-item ml-3">
                <a href="{{route('pengembalian.index')}}" class="nav-link">
                  <i class="nav-icon fas fa-plus"></i>
                  <p>Pengembalian</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>Laporan</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/kalender" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>Kalender</p>
            </a>
          </li>

          <li class="nav-header">SETTINGS</li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-wrench"></i>
              <p>Pengaturan Akun</p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="/home" class="nav-link">
              <i class="nav-icon fas fa-desktop"></i>
              <p>Log Out</p>
            </a>
          </li>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>