@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Rak</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success')}}
                    </div>
                @endif
                <a class="btn btn-primary" href="{{route('rak.create')}}">Tambah Data Rak</a>
                <table class="table table-bordered mt-2">
                  <thead>
                    <tr>
                      <th style="width: 10px">No.</th>
                      <th>Nomor Rak</th>
                      <th>Nama Rak</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($rak as $key => $wadah)
                        <tr>
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $wadah->no_rak }} </td>
                            <td> {{ $wadah->nama_rak }} </td>
                            <td style="display: flex;">
                                <a href="{{route('rak.show', ['rak' => $wadah -> id])}}" class="btn btn-info btn-sm mr-1">Show</a>
                                <a href="{{route('rak.edit', ['rak' => $wadah -> id])}}" class="btn btn-default btn-sm mr-1">Edit</a>      
                                <form action="{{route('rak.destroy', ['rak' => $wadah -> id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>                
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center"> Tidak Ada Data </td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>
    </div>
@endsection