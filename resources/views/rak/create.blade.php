@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3 mr-3">
    <div class="card-header px-5">
        <h3 class="card-title">Buat Data Rak</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/rak" method="POST">
    @csrf
        <div class="card-body px-5">
            <div class="form-group">
                <label for="no_rak">Nomor Rak</label>
                <input type="text" class="form-control" id="no_rak" name="no_rak" value="{{old('no_rak', '')}}" placeholder="Nomor Rak...">
                @error('isbn')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama_rak">Nama Rak</label>
                <input type="text" class="form-control" id="nama_rak" name="nama_rak" value="{{old('nama_rak', '')}}" placeholder="Nama Rak ...">
                @error('nama_rak')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
    <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat</button>
        </div>
    </form>
</div>
@endsection