@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3 mr-3">
    <div class="card-header px-5">
        <h3 class="card-title">Buat Data Rak</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/rak" method="POST">
    @csrf
    @method('PUT')
        <div class="card-body px-5">
            <div class="form-group">
                <label for="tanggal_kembali">Tanggal Pengembalian</label>
                <input type="date" id="tanggal_kembali" name="tanggal_kembali" value="{{old('tanggal_kembali', '')}}">
                @error('tanggal_kembali')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="denda">Denda</label>
                <input type="text" class="form-control" id="denda" name="denda" value="{{old('denda', '')}}" placeholder="Denda ...">
                @error('isbn')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi_denda">Deskripsi Denda</label>
                <input type="text" class="form-control" id="deskripsi_denda" name="deskripsi_denda" value="{{old('deskripsi_denda', '')}}" placeholder="Deskripsi Denda ...">
                @error('deskripsi_denda')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="anggota">Anggota</label>
                <select name="anggota_id" class="form-control">
                    <option>--Pilih--</option>
                    @foreach($anggota2 as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
            </div>
    <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat</button>
        </div>
    </form>
</div>
@endsection