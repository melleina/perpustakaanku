@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-5"> 
    <h5> Anggota </h5>
    <p> {{$pengembalian->anggota_id}} </p>
    <h5> Buku </h5>
    <p> {{$pengembalian->buku_id}} </p>
    <h5> Tanggal Pengembalian </h5>
    <p> {{$pengembalian->tanggal_kembali}} </p>
    <h5> Denda </h5>
    <p> {{$pengembalian->denda}} </p>
    <h5> Deskripsi Denda </h5>
    <p> {{$pengembalian->deskripsi_denda}} </p>
</div>
    
@endsection