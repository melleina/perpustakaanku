@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3 mr-3">
    <div class="card-header px-5">
        <h3 class="card-title">Edit Data Kategori</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('PUT')
        <div class="card-body px-5">
            <div class="form-group">
                <label for="nama_kategori">Nama Kategori</label>
                <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" value="{{old('nama_kategori', '')}}" placeholder="Nama Kategori ...">
                @error('isbn')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
    <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat</button>
        </div>
    </form>
</div>
@endsection