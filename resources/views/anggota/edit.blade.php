@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3 mr-3">
    <div class="card-header px-5">
        <h3 class="card-title">Buat Data Anggota</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/anggota/{{$anggota->id}}" method="POST">
    @csrf
    @method('PUT')
        <div class="card-body px-5">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama', '')}}" placeholder="Nama ...">
                @error('isbn')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="jenis_kelamin">Jenis Kelamin (Laki-laki/Perempuan)</label>
                <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="{{old('jenis_kelamin', '')}}" placeholder="Jenis Kelamin ...">
                @error('jenis_kelamin')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tgl_lahir">Tanggal Lahir</label><br>
                <input type="date" id="tgl_lahir" name="tgl_lahir" value="{{old('tgl_lahir', '')}}">
                @error('tgl_lahir')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="foto_profil">Foto Profil</label>
                <div class="col-md-6 mr-3">
                    <div class="card">
                        <div class="card-body">
                        <h4 class="card-title">Upload image</h4>
                        <p class="card-text">
                            <form action="/anggota/upload" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="file" name="image" class="form-control">
                                <br/>
                                <button type="submit" class="btn btn-primary form-control">Upload</button>
                            </form>
                        </p>
                        </div>
                    </div>
                </div>
                @error('foto_profil')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="status">Status</label>
                <input type="text" class="form-control" id="status" name="status" value="{{old('status', '')}}" placeholder="Status ...">
                @error('status')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
    <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat</button>
        </div>
    </form>
</div>
@endsection