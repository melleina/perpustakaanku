@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Anggota</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success')}}
                    </div>
                @endif
                <a class="btn btn-primary" href="{{route('anggota.create')}}">Tambah Data Anggota</a>
                <table class="table table-bordered mt-2">
                  <thead>
                    <tr>
                      <th style="width: 10px">No.</th>
                      <th>Nama</th>
                      <th>Jenis Kelamin</th>
                      <th>Tanggal Lahir</th>
                      <th>Foto Profil</th>
                      <th>Status</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($anggota as $key => $member)
                        <tr>
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $member->nama }} </td>
                            <td> {{ $member->jenis_kelamin }} </td>
                            <td> {{ $member->tgl_lahir }} </td>
                            <td> {{ $member->null }} </td>
                            <td> {{ $member->status }} </td>
                            <td style="display: flex;">
                                <a href="{{route('anggota.show', ['anggotum' => $member -> id])}}" class="btn btn-info btn-sm mr-1">Show</a>
                                <a href="{{route('anggota.edit', ['anggotum' => $member -> id])}}" class="btn btn-default btn-sm mr-1">Edit</a>      
                                <form action="{{route('anggota.destroy', ['anggotum' => $member -> id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>                
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7" align="center"> Tidak Ada Data </td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>
    </div>
@endsection