@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Buku</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success')}}
                    </div>
                @endif
                <a class="btn btn-primary" href="{{route('buku.create')}}">Tambah Data Buku</a>
                <table class="table table-bordered mt-2">
                  <thead>
                    <tr>
                      <th style="width: 10px">No.</th>
                      <th>Nomor ISBN</th>
                      <th>Nama Buku</th>
                      <th>Nama Pengarang</th>
                      <th>Tahun Terbit</th>
                      <th>Status</th>
                      <th>Rak</th>
                      <th>Kategori</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($buku as $key => $book)
                        <tr>
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $book->isbn }} </td>
                            <td> {{ $book->nama_buku }} </td>
                            <td> {{ $book->nama_pengarang }} </td>
                            <td> {{ $book->tahun_terbit }} </td>
                            <td> {{ $book->status_buku }} </td>
                            <td> {{ $book->rak_id }} </td>
                            <td> {{ $book->kategori_id }} </td>
                            <td style="display: flex;"> 
                                <a href="{{route('buku.show', ['buku' => $book -> id])}}" class="btn btn-info btn-sm mr-1">Show</a>
                                <a href="{{route('buku.edit', ['buku' => $book -> id])}}" class="btn btn-default btn-sm mr-1">Edit</a>      
                                <form action="{{route('buku.destroy', ['buku' => $book -> id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>                
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9" align="center"> Tidak Ada Data </td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>
    </div>
@endsection