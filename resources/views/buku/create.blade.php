@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3 mr-3">
    <div class="card-header px-5">
        <h3 class="card-title">Buat Data Buku</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/buku" method="POST">
    @csrf
        <div class="card-body px-5">
            <div class="form-group">
                <label for="isbn">Nomor ISBN</label>
                <input type="text" class="form-control" id="isbn" name="isbn" value="{{old('isbn', '')}}" placeholder="ISBN ...">
                @error('isbn')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama_buku">Nama Buku</label>
                <input type="text" class="form-control" id="nama_buku" name="nama_buku" value="{{old('nama_buku', '')}}" placeholder="Nama Buku ...">
                @error('nama_buku')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama_pengarang">Nama Pengarang</label>
                <input type="text" class="form-control" id="nama_pengarang" name="nama_pengarang" value="{{old('nama_pengarang', '')}}" placeholder="Nama Pengarang ...">
                @error('nama_pengarang')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tahun_terbit">Tahun Terbit</label>
                <input type="text" class="form-control" id="tahun_terbit" name="tahun_terbit" value="{{old('tahun_terbit', '')}}" placeholder="Tahun Terbit ...">
                @error('tahun_terbit')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="status_buku">Status</label>
                <input type="text" class="form-control" id="status_buku" name="status_buku" value="{{old('status_buku', '')}}" placeholder="Status ...">
                @error('status_buku')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="rak">Rak</label>
                <select name="rak_id" class="form-control">
                    <option>--Pilih--</option>
                    @foreach($rakbuku as $item)
                    <option value="{{$item->id}}">{{$item->nama_rak}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="kategori">Kategori</label>
                <select name="kategori_id" class="form-control">
                    <option>--Pilih--</option>
                    @foreach($catname as $item)
                    <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                    @endforeach
                </select>
            </div>
    <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat</button>
        </div>
    </form>
</div>
@endsection