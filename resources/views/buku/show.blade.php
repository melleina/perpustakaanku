@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-5"> 
    <h5> Nomor ISBN </h5>
    <p> {{$buku->isbn}} </p>
    <h5> Nama Buku </h5>
    <p> {{$buku->nama_buku}} </p>
    <h5> Nama Pengarang </h5>
    <p> {{$buku->nama_pengarang}} </p>
    <h5> Tahun Terbit </h5>
    <p> {{$buku->tahun_terbit}} </p>
    <h5> Status </h5>
    <p> {{$buku->status_buku}} </p>
    <h5> Rak </h5>
    <p> {{$buku->rak_id}} </p>
    <h5> Kategori </h5>
    <p> {{$buku->kategori_id}} </p>
</div>
    
@endsection