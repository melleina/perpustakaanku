<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rak extends Model
{
    protected $table = "rak";
    protected $fillable = ["no_rak", "nama_rak"]; //kolom yg mau diisi
    protected $guarded = []; //kolom yg gaboleh diisi
}
