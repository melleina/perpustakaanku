<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rak;

class RakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rak = Rak::all();
        return view('rak.index', compact('rak'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rak.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_rak' => 'required|unique:rak',
            'nama_rak' => 'required',
        ]);

        $rak = Rak::create([
            "no_rak" => $request["no_rak"],
            "nama_rak" => $request["nama_rak"]
        ]);

        return redirect('/rak')->with('success', 'Data Rak Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rak = Rak::find($id);
        return view('rak.show', compact('rak'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rak = Rak::find($id);
        return view('rak.edit', compact('rak'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rak = Rak::where('id', $id)->update([
            "no_rak" => $request["no_rak"],
            "nama_rak" => $request["nama_rak"]
        ]);

        return redirect('/rak')->with('success', 'Berhasil Update Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Rak::destroy($id);
        return redirect('/rak')->with('success', 'Berhasil Menghapus Data');
    }
}
