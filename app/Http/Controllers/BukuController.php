<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\Rak;
use App\Kategori;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rakbuku = Rak::all();
        $catname = Kategori::all();
        return view('buku.create', compact('rakbuku', 'catname'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isbn' => 'required|unique:buku',
            'nama_buku' => 'required',
            'nama_pengarang' => 'required',
            'tahun_terbit' => 'required',
            'status_buku' => 'required',
            'rak_id' => 'required',
            'kategori_id' => 'required'
        ]);

        $buku = Buku::create([
            "isbn" => $request["isbn"],
            "nama_buku" => $request["nama_buku"],
            "nama_pengarang" => $request["nama_pengarang"],
            "tahun_terbit" => $request["tahun_terbit"],
            "status_buku" => $request["status_buku"],
            "rak_id" => $request["rak_id"],
            "kategori_id" => $request["kategori_id"]
        ]);

        return redirect('/buku')->with('success', 'Data Buku Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::find($id);
        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::find($id);
        $rakbuku = Rak::all();
        $catname = Kategori::all();
        return view('buku.edit', compact('buku', 'rakbuku', 'catname'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $buku = Buku::where('id', $id)->update([
            "isbn" => $request["isbn"],
            "nama_buku" => $request["nama_buku"],
            "nama_pengarang" => $request["nama_pengarang"],
            "tahun_terbit" => $request["tahun_terbit"],
            "status_buku" => $request["status_buku"]
        ]);

        return redirect('/buku')->with('success', 'Berhasil Update Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Buku::destroy($id);
        return redirect('/buku')->with('success', 'Berhasil Menghapus Data');
    }
}
