<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Foto;

class ImageController extends Controller
{
    public function upload(){
        $foto = Foto::all();
        return view('upload', ['foto' => $foto]);
    }

    public function store(Request $request){
        $extension = $request->file('imgupload')->extension();
        $imgname = date('dmyHis').'.'.$extension;
        $this->validate($request, ['imgupload'=> 'required|file|max:5000']);
        $path = Storage::putFileAs('public/images', $request->file('imgupload'), $imgname);
        Foto::upload(['path' => $imgname]);
        return redirect()->back()->withSuccess("Image success Uploaded in " . $path);
    }
}
