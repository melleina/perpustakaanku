<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anggota;

class AnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota = Anggota::all();
        return view('anggota.index', compact('anggota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('anggota.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:anggota',
            'jenis_kelamin' => 'required',
            'tgl_lahir' => 'required',
            'status' => 'required'
        ]);

        $anggota = Anggota::create([
            "nama" => $request["nama"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "tgl_lahir" => $request["tgl_lahir"],
            "status" => $request["status"]
        ]);

        return redirect('/anggota')->with('success', 'Data Anggota Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $anggota = Anggota::find($id);
        return view('anggota.show', compact('anggota'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anggota = Anggota::find($id);
        return view('anggota.edit', compact('anggota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $anggota = Anggota::where('id', $id)->update([
            "nama" => $request["nama"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "tgl_lahir" => $request["tgl_lahir"],
            //"foto_profil" => $request["foto_profil"],
            "status" => $request["status"]
        ]);

        return redirect('/anggota')->with('success', 'Berhasil Update Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Anggota::destroy($id);
        return redirect('/anggota')->with('success', 'Berhasil Menghapus Data');
    }

    //public function upload(Request $request){
      //  $file = $request->file('image');
        //$nama_file = $file->getClientOriginalName();
        //$extension = $file->getClientOriginalExtension();
        //$size = $file->getSize();
        //$path = 'storage';
        //$file->move($path,$file->getClientOriginalName());
        //return redirect()->back->withSuccess('success','Foto Berhasil Diupload');
    //}
}
