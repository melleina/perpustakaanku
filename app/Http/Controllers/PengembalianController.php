<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengembalian;
use App\Anggota;
use App\Buku;

class PengembalianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengembalian = Pengembalian::all();
        return view('pengembalian.index', compact('pengembalian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buku2 = Buku::all();
        $anggota2 = Anggota::all();
        return view('pengembalian.create', compact('buku2', 'anggota2'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal_kembali' => 'required',
            'denda' => 'required',
            'deskripsi_denda' => 'required',
            'anggota_id' => 'required',
            'buku_id' => 'required'
        ]);

        $pengembalian = Pengembalian::create([
            "tanggal_kembali" => $request["tanggal_kembali"],
            "denda" => $request["denda"],
            "deskripsi_denda" => $request["deskripsi_denda"],
            "anggota_id" => $request["anggota_id"],
            "buku_id" => $request["buku_id"]
        ]);

        return redirect('/pengembalian')->with('success', 'Data Pengembalian Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengembalian = Pengembalian::find($id);
        return view('pengembalian.show', compact('pengembalian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengembalian = Pengembalian::find($id);
        return view('pengembalian.edit', compact('pengembalian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pengembalian = Pengembalian::where('id', $id)->update([
            "tanggal_kembali" => $request["tanggal_kembali"],
            "denda" => $request["denda"],
            "deskripsi_denda" => $request["deskripsi_denda"]
        ]);

        return redirect('/pengembalian')->with('success', 'Berhasil Update Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pengembalian::destroy($id);
        return redirect('/pengembalian')->with('success', 'Berhasil Menghapus Data');
    }
}
