<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;
use App\Anggota;
use App\Buku;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peminjaman = Peminjaman::all();
        return view('peminjaman.index', compact('peminjaman'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buku1 = Buku::all();
        $anggota1 = Anggota::all();
        return view('peminjaman.create', compact('buku1', 'anggota1'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal_pinjam' => 'required',
            'tanggal_pengembalian' => 'required',
            'buku_id' => 'required',
            'anggota_id' => 'required'
        ]);

        $peminjaman = Peminjaman::create([
            "tanggal_pinjam" => $request["tanggal_pinjam"],
            "tanggal_pengembalian" => $request["tanggal_pengembalian"],
            "buku_id" => $request["buku_id"],
            "anggota_id" => $request["anggota_id"]
        ]);

        return redirect('/peminjaman')->with('success', 'Data Peminjaman Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peminjaman = Peminjaman::find($id);
        return view('peminjaman.show', compact('peminjaman'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peminjaman = Peminjaman::find($id);
        return view('peminjaman.edit', compact('peminjaman'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $peminjaman = Peminjaman::where('id', $id)->update([
            "tanggal_pinjam" => $request["tanggal_pinjam"],
            "tanggal_pengembalian" => $request["tanggal_pengembalian"]
        ]);

        return redirect('/peminjaman')->with('success', 'Berhasil Update Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Peminjaman::destroy($id);
        return redirect('/peminjaman')->with('success', 'Berhasil Menghapus Data');
    }
}
