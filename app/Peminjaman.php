<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = "peminjaman";
    protected $fillable = ["tanggal_kembali", "denda", "deskripsi_denda", "anggota_id", "buku_id"]; //kolom yg mau diisi
    protected $guarded = []; //kolom yg gaboleh diisi
}
