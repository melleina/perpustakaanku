<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = "buku";
    protected $fillable = ["isbn", "nama_buku", "nama_pengarang", "tahun_terbit", "status_buku", "rak_id", "kategori_id"]; //kolom yg mau diisi
    protected $guarded = []; //kolom yg gaboleh diisi

    public function rak()
    {
        return $this->belongsTo('App\Rak');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }
}
