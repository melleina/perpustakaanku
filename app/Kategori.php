<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "kategori";
    protected $fillable = ["nama_kategori"]; //kolom yg mau diisi
    protected $guarded = []; //kolom yg gaboleh diisi
}
