<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    protected $table = "anggota";
    protected $fillable = ["nama", "jenis_kelamin", "tgl_lahir", "status"]; //kolom yg mau diisi
    protected $guarded = ["foto_profil"]; //kolom yg gaboleh diisi
}
