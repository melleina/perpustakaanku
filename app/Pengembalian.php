<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    protected $table = "pengembalian";
    protected $fillable = ["isbn", "nama_buku", "nama_pengarang", "tahun_terbit", "status_buku", "rak_id", "kategori_id"]; //kolom yg mau diisi
    protected $guarded = []; //kolom yg gaboleh diisi
}
