<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('adminlte.dashboard');
});

Route::get('/kalender', function () {
    return view('kalender');
});

Route::resource('buku', 'BukuController')->middleware('auth');

Route::resource('anggota', 'AnggotaController')->middleware('auth');

Route::resource('kategori', 'KategoriController')->middleware('auth');

Route::resource('rak', 'RakController')->middleware('auth');

Route::resource('peminjaman', 'PeminjamanController')->middleware('auth');

Route::resource('pengembalian', 'PengembalianController')->middleware('auth');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
